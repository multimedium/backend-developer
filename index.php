<?php

require 'vendor/autoload.php';

$token = chr(108).chr(51).chr(51).chr(55).chr(45).chr(100).chr(51)
        .chr(118).chr(51).chr(108).chr(48).chr(112).chr(51).chr(114);

$client = new GuzzleHttp\Client();
$res = $client->request('POST', 'https://work.at.multimedium.be', [
    'headers' => [
        'x-secret-token' => $token
    ]
]);

echo $res->getBody();